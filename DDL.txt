create schema gymdb collate utf8_general_ci;

create table trainer
(
	ID int auto_increment,
	EXPERIENCE_YEARS double null,
	ADDITIONAL_INFO varchar(250) null,
	user_id bigint null,
	constraint USER_ID_UNIQUE
		unique (ID)
);

create index ID_idx
	on trainer (ID);

alter table trainer
	add primary key (ID);

create table user
(
	ID int auto_increment,
	LOGIN varchar(45) not null,
	PASSWORD varchar(45) not null,
	NAME varchar(45) null,
	SURNAME varchar(45) null,
	EMAIL varchar(45) not null,
	ROLE varchar(45) not null,
	constraint ID_UNIQUE
		unique (ID),
	constraint LOGIN_UNIQUE
		unique (LOGIN),
	constraint user_EMAIL_uindex
		unique (EMAIL)
);

alter table user
	add primary key (ID);

create table trainer_review
(
	ID int auto_increment,
	TRAINER_ID int not null,
	VISITOR_ID int not null,
	RATING set('1', '2', '3', '4', '5') not null,
	REVIEW varchar(250) not null,
	constraint ID_UNIQUE
		unique (ID),
	constraint REVIEWER_ID
		foreign key (VISITOR_ID) references user (ID),
	constraint trainer_review_user_ID_fk
		foreign key (TRAINER_ID) references user (ID)
			on update cascade on delete cascade
);

create index REVIEWER_ID_idx
	on trainer_review (VISITOR_ID);

alter table trainer_review
	add primary key (ID);

create table training_date
(
	ID int auto_increment
		primary key,
	TRAINER_ID int not null,
	VISITOR_ID int null,
	TRAINING_DATE timestamp default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP,
	constraint TRAINER_ID
		foreign key (TRAINER_ID) references user (ID),
	constraint VISITOR_ID
		foreign key (VISITOR_ID) references user (ID)
);

create index TRAINER_ID_idx
	on training_date (TRAINER_ID);

create index VISITOR_ID_idx
	on training_date (VISITOR_ID);

