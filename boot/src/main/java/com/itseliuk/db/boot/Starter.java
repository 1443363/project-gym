package com.itseliuk.db.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("com")
@SpringBootApplication
public class Starter  {


    public static void main(String[] args) {
        SpringApplication.run(Starter.class, args);
    }

}
