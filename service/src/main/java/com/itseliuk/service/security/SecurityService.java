package com.itseliuk.service.security;

public interface SecurityService {

    String findLoggedInUsername();

    void autologin(String login, String password);
}
