package com.itseliuk.service.security;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    private final Logger logger = LoggerFactory.getLogger(SpringSecurityConfig.class);
    private AuthencationProviderImpl authencationProvider;

    @Autowired
    public void setAuthencationProvider(AuthencationProviderImpl authencationProvider) {
        this.authencationProvider = authencationProvider;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }

    @Override
    protected void configure(HttpSecurity http) {
        try {
            http
                    .csrf().disable()
                    .authorizeRequests()
                    .antMatchers("/views", "/views/admin/**").hasRole("admin")
                    .antMatchers("/views", "/views/visitor/**").hasRole("visitor")
                    .antMatchers("/views", "/views/trainer/**").hasRole("trainer")
                    .antMatchers("/").permitAll()
                    .anyRequest().authenticated()
                    .and().formLogin().defaultSuccessUrl("/views/user", true);
        } catch (Exception e) {
            logger.error(e.toString());
        }
    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(authencationProvider);
    }


}
