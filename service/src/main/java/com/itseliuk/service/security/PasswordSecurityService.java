package com.itseliuk.service.security;

public interface PasswordSecurityService {
    String encrypt(String passwordToEncr);
    String decrypt(String passwordToDecr);
}
