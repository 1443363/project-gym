package com.itseliuk.service.security;

import com.itseliuk.db.entities.UserEntity;
import com.itseliuk.service.spring.UserServiceSpringImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class AuthencationProviderImpl implements AuthenticationProvider {

    private final Logger logger = LoggerFactory.getLogger(AuthencationProviderImpl.class);

    @Autowired
    private UserServiceSpringImpl userServiceSpring;

    @Override
    public Authentication authenticate(Authentication authentication)
            throws AuthenticationException {
        String userName = authentication.getName();
        String password = authentication.getCredentials().toString();
        UserEntity user = userServiceSpring.getUserByLogin(userName);

        if (user == null) {
            logger.error("Unknown user " + userName);
            throw new BadCredentialsException("Unknown user " + userName);
        }

        if (!password.equals(PasswordSecurityServiceImpl.getInstance().decrypt(user.getPassword()))) {
            logger.error("Bad password");
            throw new BadCredentialsException("Bad password");
        }

        UserDetails principal = User.builder()
                .username(user.getLogin())
                .password(user.getPassword())
                .roles(user.getRole())
                .build();

        return new UsernamePasswordAuthenticationToken(
                principal, password, principal.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
