package com.itseliuk.service.spring;
import com.itseliuk.db.entities.TrainerReviewEntity;
import com.itseliuk.db.entities.UserEntity;
import com.itseliuk.db.repository.TrainerReviewRepository;
import com.itseliuk.service.exceptions.IncorrectIdException;
import com.itseliuk.service.exceptions.NoRecordsException;
import com.itseliuk.service.security.SpringSecurityConfig;
import com.itseliuk.service.validator.TrainerReviewValidatorImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TrainerReviewServiceSpringImpl implements DaoSpringService<TrainerReviewEntity>{

    private final Logger logger = LoggerFactory.getLogger(TrainerReviewServiceSpringImpl.class);
    private TrainerReviewRepository repo;
    private TrainerReviewValidatorImpl trainerReviewValidator;

    @Autowired
    public void setRepo(TrainerReviewRepository repo) {
        this.repo = repo;
    }

    @Autowired
    public void setTrainerReviewValidator(TrainerReviewValidatorImpl trainerReviewValidator) {
        this.trainerReviewValidator = trainerReviewValidator;
    }

    @Override
    public TrainerReviewEntity get(Long id) {
        logger.info("Getting trainer review");
        if (!repo.findById(id).isPresent()) {
            try {
                throw new IncorrectIdException("There is no trainer review with such id.");
            } catch (IncorrectIdException incorrectIdException) {
                logger.error(incorrectIdException.toString());
            }
        }
        return repo.findById(id).get();
    }

    @Override
    public List<TrainerReviewEntity> getAll() {
        logger.info("Getting all trainers review");
        List<TrainerReviewEntity> reviews = new ArrayList<>();
        repo.findAll().forEach(reviews::add);

        if (reviews.size() == 0) {
            try {
                throw new NoRecordsException("There are no records in TrainerReview table at the moment.");
            } catch (NoRecordsException noRecordsException) {
                logger.error(noRecordsException.toString());
            }
        }

        return reviews;
    }

    @Override
    public Long save(TrainerReviewEntity item) {
        logger.info("Saving trainer review..");
        return repo.save(item).getId();
    }

    public Long save(Long trainerId, UserEntity visitor, int rating, String comment) {
        logger.info("Saving trainer review..");
        TrainerReviewEntity review = trainerReviewValidator.validate(trainerId, visitor, rating, comment);
        if (review == null) {
            return 0L;
        }
        return repo.save(review).getId();
    }

    @Override
    public TrainerReviewEntity update(TrainerReviewEntity item) {
        logger.info("Updating trainer review");
        return repo.save(item);
    }

    @Override
    public void delete(String id) {
        logger.info("Deleteing trainer review");
        if (!repo.existsById(Long.parseLong(id))) {
            try {
                throw new IncorrectIdException("There is no trainer review with such id.");
            } catch (IncorrectIdException e) {
                logger.error(e.toString());
            }
        } else {
            repo.deleteById(Long.parseLong(id));
        }
    }

    public Page<TrainerReviewEntity> getAllTrainerReviews(Long trainerId, int pageNumber) {
        logger.info("Getting all reviews for trainer");
        Pageable pageable = PageRequest.of(pageNumber - 1, 2);
        return repo.findAllByTrainerId(trainerId, pageable);
    }
}
