package com.itseliuk.service.spring;
import com.itseliuk.db.entities.TrainingDateEntity;
import com.itseliuk.db.entities.UserEntity;
import com.itseliuk.db.repository.TrainingDateRepository;
import com.itseliuk.service.exceptions.IncorrectDateRegistrationException;
import com.itseliuk.service.exceptions.IncorrectIdException;
import com.itseliuk.service.exceptions.NoRecordsException;
import com.itseliuk.service.validator.DataValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.List;

@Service
public class TrainingDateServiceSpringImpl implements DaoSpringService<TrainingDateEntity> {

    private final Logger logger = LoggerFactory.getLogger(TrainingDateServiceSpringImpl.class);
    TrainingDateRepository repo;
    DataValidator<Timestamp> validator;

    @Autowired
    public void setRepo(TrainingDateRepository repo) {
        this.repo = repo;
    }

    @Autowired
    public void setValidator(DataValidator<Timestamp> validator) {
        this.validator = validator;
    }

    @Override
    public TrainingDateEntity get(Long id) {
        logger.info("Getting training date..");
        if (!repo.findById(id).isPresent()) {
            try {
                throw new IncorrectIdException("There is no training date with such id.");
            } catch (IncorrectIdException incorrectIdException) {
                logger.error(incorrectIdException.toString());
            }
        }
        return repo.findById(id).get();
    }

    @Override
    public List<TrainingDateEntity> getAll() {
        logger.info("Getting all training dates");
        if (repo.findAll().isEmpty()) {
            try {
                throw new NoRecordsException("There are no records in TrainingDate table at the moment.");
            } catch (NoRecordsException noRecordsException) {
                logger.error(noRecordsException.toString());
            }
        }
        return repo.findAll();
    }

    public Iterable<TrainingDateEntity> getAllWithPaginationAndSorting(int pageNumber, String sortField, String sortDir) {
        logger.info("Getting all training dates");
        Sort sort = Sort.by(sortField).ascending();
        sort = sortDir.equals("asc") ? sort.ascending() : sort.descending();
        Pageable pageable = PageRequest.of(pageNumber - 1, 7, sort);
        return repo.findAll(pageable);
    }

    @Override
    public Long save(TrainingDateEntity item) {
        logger.info("Saving training date..");
        return repo.save(item).getId();
    }

    public Long save(UserEntity trainer, UserEntity visitor, String dateStringFormat) {
        logger.info("Saving training date..");
        Timestamp date = null;
        try {
            date = validator.validateTrainingDate(trainer, dateStringFormat);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IncorrectDateRegistrationException e) {
            logger.error(e.toString());
        }

        if (date.getTime() == 0) {
            return 0L;
        }

        TrainingDateEntity trainingDateEntity = new TrainingDateEntity(trainer, visitor,
                date);

        return repo.save(trainingDateEntity).getId();
    }

    @Override
    public TrainingDateEntity update(TrainingDateEntity item) {
        logger.info("Updating training date..");
        return repo.save(item);
    }

    @Override
    public void delete(String id) {
        logger.info("Deleting training date..");
        if (!repo.existsById(Long.parseLong(id))) {
            try {
                throw new IncorrectIdException("There is no training date with such id.");
            } catch (IncorrectIdException e) {
                logger.error(e.toString());
            }
        } else {
            repo.deleteById(Long.parseLong(id));
        }
    }
}
