package com.itseliuk.service.spring;

import com.itseliuk.db.entities.TrainerEntity;
import com.itseliuk.db.repository.TrainerRepository;
import com.itseliuk.service.exceptions.IncorrectIdException;
import com.itseliuk.service.exceptions.NoRecordsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class TrainerServiceSpringImpl implements DaoSpringService<TrainerEntity> {

    private final Logger logger = LoggerFactory.getLogger(TrainerServiceSpringImpl.class);
    private TrainerRepository repo;

    @Autowired
    private TrainerServiceSpringImpl(TrainerRepository repo) {
        this.repo = repo;
    }

    private TrainerServiceSpringImpl(){}

    @Override
    public TrainerEntity get(Long id) {
        logger.info("Getting trainer review..");
        if (!repo.findById(id).isPresent()) {
            try {
                throw new IncorrectIdException("There is no trainer with such id.");
            } catch (IncorrectIdException incorrectIdException) {
                logger.error(incorrectIdException.toString());
            }
        }
        return repo.findById(id).get();
    }

    public Page<TrainerEntity> getAllWithPaginationAndSorting(int pageNumber, String sortField, String sortDir) {
        logger.info("Getting all trainers..");
        Sort sort = Sort.by(sortField).ascending();
        sort = sortDir.equals("asc") ? sort.ascending() : sort.descending();
        Pageable pageable = PageRequest.of(pageNumber - 1, 7, sort);
        return repo.findAll(pageable);
    }

    @Override
    public Iterable<TrainerEntity> getAll() {
        logger.info("Getting all trainers..");
        if (!repo.findAll().iterator().hasNext()) {
            try {
                throw new NoRecordsException("There are no records in Trainer table at the moment.");
            } catch (NoRecordsException noRecordsException) {
                logger.error(noRecordsException.toString());
            }
        }
        return repo.findAll();
    }


    @Override
    public Long save(TrainerEntity item) {
        logger.info("Saving trainer..");
        return repo.save(item).getId();
    }


    @Override
    public TrainerEntity update(TrainerEntity item) {
        logger.info("Updating trainer..");
        return repo.save(item);
    }

    @Override
    public void delete(String id) {
        logger.info("Deleting trainer..");
        if (!repo.existsById(Long.parseLong(id))) {
            try {
                throw new IncorrectIdException("There is no trainer with such id.");
            } catch (IncorrectIdException e) {
                logger.error(e.toString());
            }
        } else {
            repo.deleteById(Long.parseLong(id));
        }
    }
}
