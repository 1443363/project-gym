package com.itseliuk.service.spring;

import com.itseliuk.db.repository.UserRepository;
import com.itseliuk.db.entities.TrainerEntity;
import com.itseliuk.db.entities.UserEntity;
import com.itseliuk.service.exceptions.IncorrectIdException;
import com.itseliuk.service.exceptions.NoRecordException;
import com.itseliuk.service.exceptions.NoRecordsException;
import com.itseliuk.service.security.PasswordSecurityServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;


@Service
public class UserServiceSpringImpl implements DaoSpringService<UserEntity> {

    Logger logger = LoggerFactory.getLogger(UserServiceSpringImpl.class);
    private UserRepository repo;

    @Autowired
    public void setRepo(UserRepository repo) {
        this.repo = repo;
    }

    @Override
    public UserEntity get(Long id) {
        if (!repo.findById(id).isPresent()) {
            try {
                throw new IncorrectIdException("There is no user with such id.");
            } catch (IncorrectIdException incorrectIdException) {
                incorrectIdException.printStackTrace();
            }
        }
        return repo.findById(id).get();
    }

    public UserEntity get(String id) {
        logger.info("Getting user..");
        if (!repo.findById(Long.parseLong(id)).isPresent()) {
            try {
                throw new IncorrectIdException("There is no user with such id.");
            } catch (IncorrectIdException incorrectIdException) {
                logger.error(incorrectIdException.toString());
            }
        }
        return repo.findById(Long.parseLong(id)).get();
    }

    @Override
    public Iterable<UserEntity> getAll() {
        logger.info("Getting all users..");
        if (!repo.findAll().iterator().hasNext()) {
            try {
                throw new NoRecordsException("There are no records in User table at the moment.");
            } catch (NoRecordsException noRecordsException) {
                logger.error(noRecordsException.toString());
            }
        }
        return repo.findAll();
    }

    public Page<UserEntity> getAllWithPaginationAndSorting(int pageNumber, String sortField, String sortDir) {
        logger.info("Getting all users..");
        Sort sort = Sort.by(sortField).ascending();
        sort = sortDir.equals("asc") ? sort.ascending() : sort.descending();
        Pageable pageable = PageRequest.of(pageNumber - 1, 7, sort);
        return repo.findAll(pageable);
    }


    @Override
    public Long save(UserEntity item) {
        logger.info("Saving user..");
        item.setPassword(PasswordSecurityServiceImpl.getInstance().encrypt(item.getPassword()));
        if (item.getRole().equals("trainer")) {
            TrainerEntity trainer = new TrainerEntity();
            item.addTrainer(trainer);
        }
        return repo.save(item).getId();
    }

    public Long save(String login, String password, String name, String surname, String
            email, String role) {
        logger.info("Saving user..");
        UserEntity item = new UserEntity(login, PasswordSecurityServiceImpl.getInstance().encrypt(password),
                name, surname, email, role);

        if (role.equals("trainer")) {
            TrainerEntity trainer = new TrainerEntity();
            item.setTrainer(trainer);
        }

        return repo.save(item).getId();
    }

    @Override
    public UserEntity update(UserEntity item) {
        logger.info("Updating user..");
        if (get(item.getId()).getRole().equals("trainer") && !item.getRole().equals("trainer")) {
            item.deleteTrainer(item.getTrainer());
        }

        if (!get(item.getId()).getRole().equals("trainer") && item.getRole().equals("trainer")) {
            TrainerEntity trainer = new TrainerEntity();
            item.setTrainer(trainer);
        }
        String dcrptPass = PasswordSecurityServiceImpl.getInstance().decrypt(item.getPassword());
        item.setPassword(PasswordSecurityServiceImpl.getInstance().encrypt(dcrptPass));
        return repo.save(item);
    }

    @Override
    public void delete(String id) {
        logger.info("Deleting user..");
        if (!repo.existsById(Long.parseLong(id))) {
            try {
                throw new IncorrectIdException("There is no user with such id.");
            } catch (IncorrectIdException e) {
                logger.error(e.toString());
            }
        } else {
            repo.deleteById(Long.parseLong(id));
        }
    }

    public String getRoleByLogin(String login) {
        logger.info("Getting user role by login..");
        if (repo.findByLogin(login) == null) {
            try {
                throw new NoRecordException("There is no record with such login");
            } catch (NoRecordException e) {
                logger.error(e.toString());
            }
        }
        return repo.findByLogin(login).getRole();
    }

    public UserEntity getUserByLogin(String login) {
        logger.info("Getting user by login..");
        if (repo.findByLogin(login) == null) {
            try {
                throw new NoRecordException("There is no record with such login");
            } catch (NoRecordException e) {
                logger.error(e.toString());
            }
        }
        return repo.findByLogin(login);
    }

    public UserEntity getUserByEmail(String email) {
        logger.info("Getting user by login..");
        if (repo.findByLogin(email) == null) {
            try {
                throw new NoRecordException("There is no record with such email");
            } catch (NoRecordException e) {
                logger.error(e.toString());
                return null;
            }
        }
        return repo.findByLogin(email);
    }
}

