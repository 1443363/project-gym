package com.itseliuk.service.spring;

public interface DaoSpringService<T> {

    T get(Long id) throws Exception;

    Iterable<T> getAll();

    Long save(T item) throws Exception;

    T update(T item) throws Exception;

    void delete(String id) throws Exception;
}
