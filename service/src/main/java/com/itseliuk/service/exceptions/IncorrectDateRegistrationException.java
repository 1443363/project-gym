package com.itseliuk.service.exceptions;

public class IncorrectDateRegistrationException extends Exception{
    public IncorrectDateRegistrationException(String message) {
        super(message);
    }

    @Override
    public String toString() {
        return "IncorrectDateRegistrationException{}: " + getMessage();
    }
}
