package com.itseliuk.service.exceptions;

public class TrainerReviewDuplicateException extends Exception{
    public TrainerReviewDuplicateException(String message) {
        super(message);
    }

    @Override
    public String toString() {
        return "TrainerReviewDuplicateException{}: " + getMessage();
    }
}
