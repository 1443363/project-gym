package com.itseliuk.service.exceptions;

public class NoRecordsException extends Exception {
    public NoRecordsException(String message) {
        super(message);
    }

    @Override
    public String toString() {
        return "NoRecordsException{}: " + getMessage();
    }
}
