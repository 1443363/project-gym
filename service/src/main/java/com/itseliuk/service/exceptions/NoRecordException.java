package com.itseliuk.service.exceptions;

public class NoRecordException extends Exception{
    public NoRecordException(String message) {
        super(message);
    }

    @Override
    public String toString() {
        return "NoRecordException{}: " + getMessage();
    }
}
