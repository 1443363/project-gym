package com.itseliuk.service.calculator;

public interface Calculator {
    String calculate(Long trainerId);
    Integer getVotesNumber(Long trainerId);
}
