package com.itseliuk.service.calculator;

import com.itseliuk.db.entities.TrainerReviewEntity;
import com.itseliuk.db.repository.TrainerReviewRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.List;



@Service
public class RatingCalculator implements Calculator{

    Logger logger = LoggerFactory.getLogger(RatingCalculator.class);
    TrainerReviewRepository repo;


    @Autowired
    public void setRepo(TrainerReviewRepository repo) {
        this.repo = repo;
    }

    @Override
    public String calculate(Long trainerId) {
        List<TrainerReviewEntity> list = repo.findByTrainerId(trainerId);
        double sum = 0;

        for (TrainerReviewEntity review: list) {
            sum += review.getRating();
        }
        sum = sum/list.size();
        logger.info("Review rating:" + sum);
        DecimalFormat df = new DecimalFormat("#.#");

        return df.format(sum);
    }

    @Override
    public Integer getVotesNumber(Long trainerId) {
        List<TrainerReviewEntity> list = repo.findByTrainerId(trainerId);
        logger.info("Votes number:" + list.size());
        return list.size();
    }
}
