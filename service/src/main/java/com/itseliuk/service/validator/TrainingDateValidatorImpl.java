package com.itseliuk.service.validator;

import com.itseliuk.db.entities.TrainingDateEntity;
import com.itseliuk.db.entities.UserEntity;
import com.itseliuk.service.exceptions.IncorrectDateRegistrationException;
import com.itseliuk.service.spring.TrainingDateServiceSpringImpl;
import com.itseliuk.service.spring.UserServiceSpringImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TrainingDateValidatorImpl implements DataValidator<Timestamp> {

    Logger logger = LoggerFactory.getLogger(TrainingDateValidatorImpl.class);
    private TrainingDateServiceSpringImpl trainingDateServiceSpring;
    private UserServiceSpringImpl userServiceSpring;

    @Autowired
    public void setTrainingDateServiceSpring(TrainingDateServiceSpringImpl trainingDateServiceSpring) {
        this.trainingDateServiceSpring = trainingDateServiceSpring;
    }

    @Autowired
    public void setUserServiceSpring(UserServiceSpringImpl userServiceSpring) {
        this.userServiceSpring = userServiceSpring;
    }

    public Timestamp validateTrainingDate(UserEntity trainer, String dateStringFormat){
        logger.info("Validation training date..");
        List<TrainingDateEntity> trainerTrainingDates = trainingDateServiceSpring.getAll().stream()
                .filter(date -> trainer.getId().equals(date.getTrainer().getId()))
                .collect(Collectors.toList());

        Date dateUtil = null;
        try {
            dateUtil = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm").parse(dateStringFormat);
        } catch (ParseException e) {
            logger.error(e.toString());
        }

        assert dateUtil != null;
        Timestamp date = new Timestamp(dateUtil.getTime());
        Timestamp dateMinusHour = new Timestamp(date.getTime() - 7200000);
        Timestamp datePlusHour = new Timestamp(date.getTime() + 7200000);

        for (TrainingDateEntity trainingDate: trainerTrainingDates) {
            if (trainingDate.getTrainingDate().after(dateMinusHour)
                    && trainingDate.getTrainingDate().before(datePlusHour)) {
                try {
                    throw new IncorrectDateRegistrationException("This date has already been booked for this trainer");
                } catch (IncorrectDateRegistrationException incorrectDateRegistrationException) {
                    logger.error(incorrectDateRegistrationException.toString());
                    return new Timestamp(0);
                }
            }
        }

        return date;
    }

    public List<TrainingDateEntity> getScheduledVisits() {
        logger.info("Getting scheduled visits..");
        User visitorCreds = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<TrainingDateEntity> scheduledVisits = null;

        UserEntity visitor = userServiceSpring.getUserByLogin(visitorCreds.getUsername());
        scheduledVisits = trainingDateServiceSpring.getAll().stream()
                .filter(date -> visitor.getId().equals(date.getVisitor().getId()))
                .collect(Collectors.toList());

        return scheduledVisits;
    }
    
}
