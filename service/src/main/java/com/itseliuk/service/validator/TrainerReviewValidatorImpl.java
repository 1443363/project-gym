package com.itseliuk.service.validator;

import com.itseliuk.db.entities.TrainerReviewEntity;
import com.itseliuk.db.entities.UserEntity;
import com.itseliuk.service.exceptions.TrainerReviewDuplicateException;
import com.itseliuk.service.spring.TrainerReviewServiceSpringImpl;
import com.itseliuk.service.spring.UserServiceSpringImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class TrainerReviewValidatorImpl{

    Logger logger = LoggerFactory.getLogger(TrainerReviewValidatorImpl.class);
    private UserServiceSpringImpl userServiceSpring;
    private TrainerReviewServiceSpringImpl trainerReviewServiceSpring;

    @Autowired
    public void setUserServiceSpring(UserServiceSpringImpl userServiceSpring) {
        this.userServiceSpring = userServiceSpring;
    }

    @Autowired
    public void setTrainerReviewServiceSpring(TrainerReviewServiceSpringImpl trainerReviewServiceSpring) {
        this.trainerReviewServiceSpring = trainerReviewServiceSpring;
    }

    public TrainerReviewEntity validate(Long trainerId, UserEntity visitor, Integer rating, String review) {
        logger.info("Validating trainer review..");
        UserEntity trainer = null;
        trainer = userServiceSpring.get(trainerId);
        List<TrainerReviewEntity> reviews = trainerReviewServiceSpring.getAll();
        boolean isDuplicate = reviews.stream()
                .anyMatch(r -> (trainerId.equals(r.getTrainer().getId()))
                        && (visitor.getId().equals(r.getVisitor().getId())));

        if (isDuplicate) {
            try {
                throw new TrainerReviewDuplicateException("You have already rated this trainer");
            } catch (TrainerReviewDuplicateException trainerReviewDuplicateException) {
                logger.error(trainerReviewDuplicateException.toString());
                return null;
            }
        } else {
            return new TrainerReviewEntity(trainer, visitor, rating, review);
        }
    }
}
