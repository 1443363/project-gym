package com.itseliuk.service.validator;

import com.itseliuk.db.entities.UserEntity;
import com.itseliuk.service.spring.UserServiceSpringImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator {

    UserServiceSpringImpl userServiceSpring;

    @Autowired
    public void setUserServiceSpring(UserServiceSpringImpl userServiceSpring) {
        this.userServiceSpring = userServiceSpring;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return UserEntity.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserEntity user = (UserEntity) o;
        if (userServiceSpring.getUserByEmail(user.getEmail()) != null) {
            errors.rejectValue("email", "", "This email already used, please enter another one.");
        }
    }
}
