package com.itseliuk.service.validator;

import com.itseliuk.db.entities.UserEntity;
import com.itseliuk.service.exceptions.IncorrectDateRegistrationException;

import java.text.ParseException;

public interface DataValidator<T> {
    T validateTrainingDate(UserEntity trainer, String dateStringFormat)
            throws ParseException, IncorrectDateRegistrationException;
}
