package com.itseliuk.db.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "trainer")
public class TrainerEntity {

    @Column(name = "EXPERIENCE_YEARS")
    private double experienceYears;

    @Column(name = "ADDITIONAL_INFO")
    private String additionalInfo;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;


    @OneToOne(cascade = CascadeType.REMOVE)
    private UserEntity user;

    public TrainerEntity() {
    }

    public TrainerEntity(Long id) {
        setId(id);
    }

    public TrainerEntity(double experienceYears, String additionalInfo) {
        this.experienceYears = experienceYears;
        this.additionalInfo = additionalInfo;
    }

    public TrainerEntity(Long id, double experienceYears, String additionalInfo) {
        setId(id);
        this.experienceYears = experienceYears;
        this.additionalInfo = additionalInfo;
    }


    public double getExperienceYears() {
        return experienceYears;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public UserEntity getUser() {
        return user;
    }

    public void addUser(UserEntity user) {
        this.user = user;
        user.setTrainer(this);
    }

    public Long getId() {
        return id;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setExperienceYears(double experienceYears) {
        this.experienceYears = experienceYears;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }


    @Override
    public String toString() {
        return "TrainerEntity{" +
                "experienceYears=" + experienceYears +
                ", additionalInfo='" + additionalInfo + '\'' +
                ", id=" + id +
                ", user=" + user +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TrainerEntity that = (TrainerEntity) o;
        return Double.compare(that.experienceYears, experienceYears) == 0 &&
                Objects.equals(additionalInfo, that.additionalInfo) &&
                Objects.equals(id, that.id) &&
                Objects.equals(user, that.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(experienceYears, additionalInfo, id, user);
    }
}
