package com.itseliuk.db.entities;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Access(AccessType.FIELD)
@Table(name = "user")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true)
    private Long id;

    @Column(name = "LOGIN", unique = true, nullable = false)
    private String login;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "NAME")
    private String name;

    @Column(name = "SURNAME")
    private String surname;

    @Column(name = "EMAIL", unique = true, nullable = false)
    private String email;

    @Column(name = "ROLE")
    private String role;

    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    private TrainerEntity trainer;

    @OneToMany(mappedBy = "trainer", cascade = CascadeType.REMOVE)
    private List<TrainerReviewEntity> ratedTrainer;

    @OneToMany(mappedBy = "visitor", cascade = CascadeType.REMOVE)
    private List<TrainerReviewEntity> visitorReviewer;

    @OneToMany(mappedBy = "trainerDate", cascade = CascadeType.REMOVE)
    private List<TrainingDateEntity> trainerDate;

    @OneToMany(mappedBy = "visitorDate", cascade = CascadeType.REMOVE)
    private List<TrainingDateEntity> visitorDate;

    public UserEntity() {
    }

    public UserEntity(long id) {
        this.id = id;
    }

    public UserEntity(String login, String password, String name, String surname, String email, String role) {
        this.login = login;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.role = role;
    }

    public UserEntity(Long id, String login, String password, String name, String surname, String email, String role) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public String getRole() {
        return role;
    }

    public void addTrainer(TrainerEntity trainer) {
        setTrainer(trainer);
        trainer.setUser(this);
    }

    public void deleteTrainer(TrainerEntity trainer) {
        setTrainer(trainer);
        trainer.setUser(this);
    }

    public TrainerEntity getTrainer() {
        return trainer;
    }

    public List<TrainerReviewEntity> getRatedTrainer() {
        return ratedTrainer;
    }

    public List<TrainerReviewEntity> getVisitorReviewer() {
        return visitorReviewer;
    }

    public List<TrainingDateEntity> getTrainerDate() {
        return trainerDate;
    }

    public List<TrainingDateEntity> getVisitorDate() {
        return visitorDate;
    }

    public void setVisitorReviewer(TrainerReviewEntity visitorReviewer) {
        this.visitorReviewer.add(visitorReviewer);
    }

    public void setRatedTrainer(TrainerReviewEntity ratedTrainer) {
        this.ratedTrainer.add(ratedTrainer);
    }

    public void setTrainer(TrainerEntity trainer) {
        if (trainer == null) {
            if (this.trainer != null) {
                this.trainer.setUser(null);
            }
        } else {
            trainer.setUser(this);
        }
        this.trainer = trainer;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setTrainerDate(TrainingDateEntity trainerDate) {
        this.trainerDate.add(trainerDate);
    }

    public void setVisitorDate(TrainingDateEntity visitorDate) {
        this.visitorDate.add(visitorDate);
    }

    @Override
    public String toString() {
        return "UserEntity{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                ", role='" + role + '\'' +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserEntity that = (UserEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(login, that.login) &&
                Objects.equals(password, that.password) &&
                Objects.equals(name, that.name) &&
                Objects.equals(surname, that.surname) &&
                Objects.equals(email, that.email) &&
                Objects.equals(role, that.role) &&
                Objects.equals(trainer, that.trainer) &&
                Objects.equals(ratedTrainer, that.ratedTrainer) &&
                Objects.equals(visitorReviewer, that.visitorReviewer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, login, password, name, surname, email, role, trainer, ratedTrainer, visitorReviewer);
    }
}
