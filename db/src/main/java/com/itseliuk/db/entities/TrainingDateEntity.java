package com.itseliuk.db.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "training_date")
public class TrainingDateEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "TRAINER_ID", unique = false)
    private UserEntity trainerDate = new UserEntity();

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "VISITOR_ID", unique = false)
    private UserEntity visitorDate = new UserEntity();

    @Column(name = "TRAINING_DATE", nullable = false)
    private Timestamp trainingDate;

    public TrainingDateEntity() {
    }

    public TrainingDateEntity(UserEntity trainerDate, UserEntity visitorDate, Timestamp trainingDate) {
        addTrainer(trainerDate);
        addVisitor(visitorDate);
        setTrainingDate(trainingDate);
    }

    public Long getId() {
        return this.id;
    }

    public Date getTrainingDate() {
        return trainingDate;
    }

    public UserEntity getTrainer() {
        return trainerDate;
    }

    public UserEntity getVisitor() {
        return visitorDate;
    }

    public void setTrainer(UserEntity trainer) {
        this.trainerDate = trainer;
    }

    public void addTrainer(UserEntity trainer) {
        setTrainer(trainer);
        trainer.setTrainerDate(this);
    }

    public void addVisitor(UserEntity visitor) {
        setVisitor(visitor);
        visitor.setVisitorDate(this);
    }

    public void setVisitor(UserEntity visitor) {
        this.visitorDate = visitor;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setTrainingDate(Timestamp trainingDate) {
        this.trainingDate = trainingDate;
    }

    @Override
    public String toString() {
        return "TrainingDateEntity{" +
                "id=" + id +
                ", trainerId=" + trainerDate.getId() +
                ", visitorId=" + visitorDate.getId() +
                ", trainingDate=" + trainingDate +
                '}';
    }
}
