package com.itseliuk.db.entities;

import javax.persistence.*;

@Entity
@Table(name = "trainer_review")
public class TrainerReviewEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private long id;

    @Column(name = "RATING")
    private int rating;

    @Column(name = "REVIEW")
    private String review;

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "TRAINER_ID", unique = true)
    private UserEntity trainer;

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "VISITOR_ID", unique = true)
    private UserEntity visitor;

    public TrainerReviewEntity() {
    }

    public TrainerReviewEntity(UserEntity trainer, UserEntity visitor, int rating, String review) {
        addTrainer(trainer);
        addVisitor(visitor);
        this.rating = rating;
        this.review = review;
    }

    public TrainerReviewEntity(long id, long trainerId, long visitorId, int rating, String review) {
        this.id = id;
//        trainer.setId(trainerId);
//        visitor.setId(visitorId);
        this.rating = rating;
        this.review = review;
    }

    public long getId() {
        return id;
    }

    public int getRating() {
        return rating;
    }

    public String getReview() {
        return review;
    }

    public UserEntity getTrainer() {
        return trainer;
    }

    public void addTrainer(UserEntity trainer) {
        setTrainer(trainer);
        trainer.setRatedTrainer(this);
    }

    public void addVisitor(UserEntity visitor) {
        setVisitor(visitor);
        visitor.setVisitorReviewer(this);
    }


    public UserEntity getVisitor() {
        return visitor;
    }

    public void setTrainer(UserEntity trainer) {
        this.trainer = trainer;
    }

    public void setVisitor(UserEntity visitor) {
        this.visitor = visitor;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public void setReview(String review) {
        this.review = review;
    }

    @Override
    public String toString() {
        return "TrainerReviewEntity{" +
                "id=" + id +
                ", rating=" + rating +
                ", review='" + review + '\'' +
                ", trainer=" + trainer.getId() +
                ", visitor=" + visitor.getId() +
                '}';
    }
}
