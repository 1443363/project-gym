package com.itseliuk.db.repository;

import com.itseliuk.db.entities.UserEntity;
import org.springframework.data.repository.PagingAndSortingRepository;;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends PagingAndSortingRepository<UserEntity, Long> {

    UserEntity findByLogin(String login);

    List<UserEntity> findByRole(String role);

    UserEntity findByEmail(String email);
}
