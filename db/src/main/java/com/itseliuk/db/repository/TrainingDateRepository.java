package com.itseliuk.db.repository;

import com.itseliuk.db.entities.TrainingDateEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TrainingDateRepository extends JpaRepository<TrainingDateEntity, Long> {
}
