package com.itseliuk.db.repository;

import com.itseliuk.db.entities.TrainerEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrainerRepository extends PagingAndSortingRepository<TrainerEntity, Long> {
}
