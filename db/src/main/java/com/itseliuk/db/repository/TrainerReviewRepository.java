package com.itseliuk.db.repository;

import com.itseliuk.db.entities.TrainerReviewEntity;
import com.itseliuk.db.entities.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface TrainerReviewRepository extends PagingAndSortingRepository<TrainerReviewEntity, Long> {
//public interface TrainerReviewRepository extends JpaRepository<TrainerReviewEntity, Long> {

    Page<TrainerReviewEntity> findAllByTrainerId(Long trainerId, Pageable pageable);

    List<TrainerReviewEntity> findByTrainerId(Long trainerId);

}
