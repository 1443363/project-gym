package com.itseliuk.db.configuration;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@org.springframework.context.annotation.Configuration
@EnableJpaRepositories("com.itseliuk.db.repository")
@EntityScan(basePackages = "com")
public class Configuration {
}
