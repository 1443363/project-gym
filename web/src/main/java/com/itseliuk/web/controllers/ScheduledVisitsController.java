package com.itseliuk.web.controllers;

import com.itseliuk.db.entities.TrainingDateEntity;
import com.itseliuk.service.spring.TrainingDateServiceSpringImpl;
import com.itseliuk.service.spring.UserServiceSpringImpl;
import com.itseliuk.service.validator.TrainingDateValidatorImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@PropertySource("classpath:pagination.sorting.properties")
public class ScheduledVisitsController {

    private TrainingDateServiceSpringImpl trainingDateServiceSpring;
    private UserServiceSpringImpl userServiceSpring;
    private TrainingDateValidatorImpl trainingDateValidator;

    @Value("${default.pagination.page}")
    Integer defaultPage;

    @Value("${default.sort.field}")
    String defaultSortField;

    @Value("${default.sort.dir}")
    String defaultSortDir;

    @Autowired
    public void setTrainingDateServiceSpring(TrainingDateServiceSpringImpl trainingDateServiceSpring) {
        this.trainingDateServiceSpring = trainingDateServiceSpring;
    }

    @Autowired
    public void setUserServiceSpring(UserServiceSpringImpl userServiceSpring) {
        this.userServiceSpring = userServiceSpring;
    }

    @Autowired
    public void setTrainingDateValidator(TrainingDateValidatorImpl trainingDateValidator) {
        this.trainingDateValidator = trainingDateValidator;
    }

    @GetMapping("/views/visitor/scheduledVisits")
    public ModelAndView getTrainerListByPage() {
        List<TrainingDateEntity> scheduledVisits = trainingDateValidator.getScheduledVisits();

        ModelAndView mav = new ModelAndView("/views/scheduledVisits");
        mav.addObject("scheduledVisits", scheduledVisits);

        return mav;
    }
}
