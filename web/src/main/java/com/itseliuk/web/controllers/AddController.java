package com.itseliuk.web.controllers;

import com.itseliuk.db.entities.UserEntity;
import com.itseliuk.service.spring.UserServiceSpringImpl;
import com.itseliuk.service.validator.TrainingDateValidatorImpl;
import com.itseliuk.service.validator.UserValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class AddController {

    private UserServiceSpringImpl userServiceSpring;
    private UserValidator userValidator;

    @Autowired
    public void setUserServiceSpring(UserServiceSpringImpl userServiceSpring) {
        this.userServiceSpring = userServiceSpring;
    }

    @Autowired
    public void setUserValidator(UserValidator userValidator) {
        this.userValidator = userValidator;
    }

    @GetMapping("/views/admin/add")
    public ModelAndView getAddWithAttr(String userName) {
        ModelAndView mav = new ModelAndView("/views/add");
        mav.addObject("userName", userName);

        return mav;
    }

    @PostMapping("/views/admin/add")
    public ModelAndView postUser(@ModelAttribute("user") UserEntity userEntity) {
        userServiceSpring.save(userEntity.getLogin(), userEntity.getPassword(), userEntity.getName(), userEntity.getSurname(),
                userEntity.getEmail(), userEntity.getRole());

        return getAddWithAttr(userEntity.getName());
    }
}
