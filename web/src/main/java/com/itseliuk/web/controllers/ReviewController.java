package com.itseliuk.web.controllers;

import com.itseliuk.db.entities.TrainerReviewEntity;
import com.itseliuk.db.entities.UserEntity;
import com.itseliuk.service.calculator.Calculator;
import com.itseliuk.service.spring.TrainerReviewServiceSpringImpl;
import com.itseliuk.service.spring.UserServiceSpringImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.Page;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@ComponentScan("com.itseliuk.services.dao.spring")
@PropertySource("classpath:pagination.sorting.properties")
public class ReviewController {

    private TrainerReviewServiceSpringImpl trainerReviewServiceSpring;
    private Calculator calculator;
    private UserServiceSpringImpl userServiceSpring;

    @Value("${default.pagination.page}")
    Integer defaultPage;

    @Autowired
    public void setTrainerReviewServiceSpring(TrainerReviewServiceSpringImpl trainerReviewServiceSpring) {
        this.trainerReviewServiceSpring = trainerReviewServiceSpring;
    }

    @Autowired
    public void setCalculator(Calculator calculator) {
        this.calculator = calculator;
    }

    @Autowired
    public void setUserServiceSpring(UserServiceSpringImpl userServiceSpring) {
        this.userServiceSpring = userServiceSpring;
    }

    @GetMapping("/views/visitor/trainerList/review/{trainerId}/{pageNumber}")
    public ModelAndView reviewPage(@PathVariable("trainerId") Long currentTrainer,
                                   @PathVariable(value = "pageNumber", required = false) int currentPage,
                                   boolean isAlreadyCommented) {
        Page<TrainerReviewEntity> allReviews = trainerReviewServiceSpring.getAllTrainerReviews(currentTrainer, currentPage);

        ModelAndView mav = new ModelAndView("/views/review");

        if(allReviews != null && allReviews.hasContent()) {
            mav.addObject("reviews", allReviews);
            long totalItems = allReviews.getTotalElements();
            int totalPages = allReviews.getTotalPages();
            int previousPage = currentPage - 1;
            int nextPage = currentPage + 1;

            mav.addObject("totalItems", totalItems);
            mav.addObject("totalPages", totalPages);
            mav.addObject("currentPage", currentPage);
            mav.addObject("previousPage", previousPage);
            mav.addObject("nextPage", nextPage);
        } else {
            mav.addObject("totalItems", 1);
            mav.addObject("totalPages", 1);
            mav.addObject("currentPage", 1);
            mav.addObject("previousPage", 1);
            mav.addObject("nextPage", 1);
        }

        mav.addObject("isAlreadyCommented", isAlreadyCommented);
        mav.addObject("votesNumber", calculator.getVotesNumber(currentTrainer));
        mav.addObject("averageRating", calculator.calculate(currentTrainer));

        mav.addObject("trainerName", userServiceSpring.get(currentTrainer).getName());
        mav.addObject("trainerSurname", userServiceSpring.get(currentTrainer).getSurname());

        return mav;
    }

    @GetMapping("/views/visitor/trainerList/review/{trainerId}")
    public ModelAndView getReviewPage(@PathVariable("trainerId") Long currentTrainer, boolean isAlreadyCommented) {
        return reviewPage(currentTrainer, defaultPage, isAlreadyCommented);
    }

    @PostMapping(value = {"/views/visitor/trainerList/review/{trainerId}/{pageNumber}",
            "/views/visitor/trainerList/review/{trainerId}"})
    public ModelAndView addComment(@PathVariable("trainerId") Long currentTrainer,
                                   @RequestParam(value = "comment", defaultValue = "0") String comment,
                                   @RequestParam(value = "stars", defaultValue = "0") int stars) {
        boolean isAlreadyCommented = false;
        User visitorCreds = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserEntity visitor = null;

        visitor = userServiceSpring.getUserByLogin(visitorCreds.getUsername());
        if(trainerReviewServiceSpring.save(currentTrainer, visitor, stars, comment).equals(0L)) {
            isAlreadyCommented = true;
        }

        ModelAndView mav = new ModelAndView("/views/visitor/trainerList");
        return reviewPage(currentTrainer, defaultPage, isAlreadyCommented);
    }
}
