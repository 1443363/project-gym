package com.itseliuk.web.controllers;

import com.itseliuk.db.entities.TrainerEntity;
import com.itseliuk.db.entities.UserEntity;
import com.itseliuk.service.spring.TrainerServiceSpringImpl;
import com.itseliuk.service.spring.TrainingDateServiceSpringImpl;
import com.itseliuk.service.spring.UserServiceSpringImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import java.util.List;

@Controller
@PropertySource("classpath:pagination.sorting.properties")
public class TrainerListController {

    private TrainerServiceSpringImpl trainerServiceSpring;
    private UserServiceSpringImpl userServiceSpring;
    private TrainingDateServiceSpringImpl trainingDateServiceSpring;

    @Value("${default.pagination.page}")
    Integer defaultPage;

    @Value("${default.sort.field}")
    String defaultSortField;

    @Value("${default.sort.dir}")
    String defaultSortDir;

    @Autowired
    public void setTrainerServiceSpring(TrainerServiceSpringImpl trainerServiceSpring) {
        this.trainerServiceSpring = trainerServiceSpring;
    }

    @Autowired
    public void setUserServiceSpring(UserServiceSpringImpl userServiceSpring) {
        this.userServiceSpring = userServiceSpring;
    }

    @Autowired
    public void setTrainingDateServiceSpring(TrainingDateServiceSpringImpl trainingDateServiceSpring) {
        this.trainingDateServiceSpring = trainingDateServiceSpring;
    }

    @GetMapping("/views/visitor/trainerList/page/{pageNumber}")
    public ModelAndView getTrainerListByPage(@PathVariable("pageNumber") Integer currentPage,
                                   @Param("sortField") String sortField,
                                   @Param("sortDir") String sortDir) {
        if (currentPage == null)
            currentPage = defaultPage;
        if(sortField == null)
            sortField = defaultSortField;
        if(sortDir == null)
            sortDir = defaultSortDir;

        Page<TrainerEntity> page = trainerServiceSpring.getAllWithPaginationAndSorting(currentPage, sortField, sortDir);
        long totalItems = page.getTotalElements();
        int totalPages = page.getTotalPages();
        int previousPage = currentPage - 1;
        int nextPage = currentPage + 1;

        List<TrainerEntity> trainers = page.getContent();

        ModelAndView mav = new ModelAndView("/views/trainerList");
        mav.addObject("trainers", trainers);
        mav.addObject("totalItems", totalItems);
        mav.addObject("totalPages", totalPages);
        mav.addObject("currentPage", currentPage);
        mav.addObject("previousPage", previousPage);
        mav.addObject("nextPage", nextPage);
        mav.addObject("sortField", sortField);
        mav.addObject("sortDir", sortDir);

        String reverseSortDir = sortDir.equals("asc") ? "desc" : "asc";
        mav.addObject("reverseSortDir", reverseSortDir);

        return mav;
    }

    @GetMapping("/views/visitor/trainerList")
    public ModelAndView getTrainerList() {
        return getTrainerListByPage(defaultPage, defaultSortField, defaultSortDir);
    }

    @PostMapping("/views/visitor/trainerList")
    public ModelAndView postUser(@RequestParam(value = "trainerId", defaultValue = "0") String trainerId,
                                 @RequestParam(value = "datepicker", defaultValue = "null") String dateStringFormat,
                                 @PathVariable(value = "pageNumber", required = false) Integer currentPage,
                                 @Param("sortField") String sortField,
                                 @Param("sortDir") String sortDir) {
        User visitorCreds = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserEntity visitor = null;
        UserEntity trainer = null;

        visitor = userServiceSpring.getUserByLogin(visitorCreds.getUsername());
        trainer = userServiceSpring.get(trainerId);

        trainingDateServiceSpring.save(trainer, visitor, dateStringFormat);

        return getTrainerListByPage(currentPage, sortField, sortDir);
    }


}
