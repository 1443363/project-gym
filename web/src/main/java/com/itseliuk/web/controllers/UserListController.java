package com.itseliuk.web.controllers;

import com.itseliuk.db.entities.UserEntity;
import com.itseliuk.service.spring.UserServiceSpringImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@ComponentScan("com.itseliuk.services.dao.spring")
@PropertySource("classpath:pagination.sorting.properties")
public class UserListController {

    private UserServiceSpringImpl userServiceSpring;

    @Value("${default.pagination.page}")
    Integer defaultPage;

    @Value("${default.sort.field}")
    String defaultSortField;

    @Value("${default.sort.dir}")
    String defaultSortDir;

    @Autowired
    public void setUserServiceSpring(UserServiceSpringImpl userServiceSpring) {
        this.userServiceSpring = userServiceSpring;
    }

    @GetMapping("/views/admin/userList/page/{pageNumber}")
    public ModelAndView listByPage(@PathVariable("pageNumber") int currentPage,
                                   @Param("sortField") String sortField,
                                   @Param("sortDir") String sortDir) {
        Page<UserEntity> page = userServiceSpring.getAllWithPaginationAndSorting(currentPage, sortField, sortDir);
        long totalItems = page.getTotalElements();
        int totalPages = page.getTotalPages();
        int previousPage = currentPage - 1;
        int nextPage = currentPage + 1;

        List<UserEntity> users = page.getContent();

        ModelAndView mav = new ModelAndView("/views/userList");
        mav.addObject("users", users);
        mav.addObject("totalItems", totalItems);
        mav.addObject("totalPages", totalPages);
        mav.addObject("currentPage", currentPage);
        mav.addObject("previousPage", previousPage);
        mav.addObject("nextPage", nextPage);
        mav.addObject("sortField", sortField);
        mav.addObject("sortDir", sortDir);

        String reverseSortDir = sortDir.equals("asc") ? "desc" : "asc";
        mav.addObject("reverseSortDir", reverseSortDir);

        return mav;
    }

    @GetMapping("/views/admin/userList")
    public ModelAndView getUsers() {
        return listByPage(defaultPage, defaultSortField, defaultSortDir);
    }

    @PostMapping(value = {"/views/admin/userList/page/{pageNumber}", "/views/admin/userList"})
    public ModelAndView postUser(@ModelAttribute("user") UserEntity userEntity,
                                 @RequestParam(value = "deleteId", defaultValue = "0") String deleteId,
                                 @PathVariable(value = "pageNumber", required = false) Integer currentPage,
                                 @Param("sortField") String sortField,
                                 @Param("sortDir") String sortDir
    ) {
        if (currentPage == null)
            currentPage = defaultPage;
        if(sortField == null)
            sortField = defaultSortField;
        if(sortDir == null)
            sortDir = defaultSortDir;


        if (deleteId.equals("0")) {
            userServiceSpring.update(userEntity);
        } else {
            userServiceSpring.delete(deleteId);
        }

        return listByPage(currentPage, sortField, sortDir);
    }
}
