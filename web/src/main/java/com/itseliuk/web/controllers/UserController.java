package com.itseliuk.web.controllers;
import com.itseliuk.db.entities.TrainingDateEntity;
import com.itseliuk.db.entities.UserEntity;
import com.itseliuk.service.spring.TrainingDateServiceSpringImpl;
import com.itseliuk.service.spring.UserServiceSpringImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class UserController {

    private UserServiceSpringImpl userServiceSpring;
    private TrainingDateServiceSpringImpl trainingDateServiceSpring;

    @Autowired
    public void setUserServiceSpring(UserServiceSpringImpl userServiceSpring) {
        this.userServiceSpring = userServiceSpring;
    }

    @Autowired
    public void setTrainingDateServiceSpring(TrainingDateServiceSpringImpl trainingDateServiceSpring) {
        this.trainingDateServiceSpring = trainingDateServiceSpring;
    }

    @GetMapping("/views/user")
    public String getUserPage(Model model) {
        User userCreds = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        List<TrainingDateEntity> scheduledVisits = null;
        UserEntity user = userServiceSpring.getUserByLogin(userCreds.getUsername());
        model.addAttribute("userName", user.getName());

        if (user.getRole().equals("trainer")) {
            scheduledVisits = trainingDateServiceSpring.getAll().stream().filter(date -> user.getId().equals(date.getTrainer().getId())).collect(Collectors.toList());
            model.addAttribute("scheduledVisits", scheduledVisits);
        }


        return "/views/user";
    }
}
